#-------------------------------------------------
#
# Project created by QtCreator 2013-02-11T17:55:02
#
#-------------------------------------------------

QT       += core network sql xml gui

QT       -= gui

TARGET = Swiftly
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    pathtreenode.cpp \
    pathtree.cpp \
    pathparser.cpp \
    incommingconnectionqueue.cpp \
    httpserver.cpp \
    httpresponse.cpp \
    httprequest.cpp \
    httpheader.cpp \
    http_parser.c \
    worker.cpp \
    webapp.cpp \
    tcpsocket.cpp \
    taskhandler.cpp \
    utilityservice.cpp \
    sessionengine.cpp \
    session.cpp \
    authentication.cpp \
    databaseengine.cpp

HEADERS += \
    pathtreenode.h \
    pathtree.h \
    pathparser.h \
    incommingconnectionqueue.h \
    httpserver.h \
    httpresponse.h \
    httprequest.h \
    httpheader.h \
    http_parser.h \
    worker.h \
    webapp.h \
    tcpsocket.h \
    taskhandler.h \
    utilityservice.h \
    sessionengine.h \
    session.h \
    authentication.h \
    databaseengine.h

OTHER_FILES += \
    todo.txt
