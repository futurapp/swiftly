#include <QCoreApplication>
#include <QtCore/QThread>
#include "httpserver.h"
#include "utilityservice.h"
#include "authentication.h"

int main(int argc, char *argv[])
{
    REGISTER_WEBAPP(Authentication);

    QCoreApplication a(argc, argv);

    HttpServer::getSingleton().start(QThread::idealThreadCount(), 8080);
    return a.exec();
}
