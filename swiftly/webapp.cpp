#include "webapp.h"
#include "pathtree.h"
#include "sessionengine.h"

WebApp::WebApp(const QString &_pathSpace,QObject *parent )
    :QObject(parent),
      pathSpace(_pathSpace)
{

}

bool WebApp::addGetHandler(const QString &_path,const QString &handlerName)
{
    QString path=_path;

    if(!pathSpace.isNull() && !pathSpace.isEmpty())
        path='/' + pathSpace + _path;

    QString functionName=handlerName;


    return pathTree->registerAPath(path,this,functionName.append("(HttpRequest&,HttpResponse&)"),PathTreeNode::GET);
}


bool WebApp::addPostHandler(const QString &_path,const QString &handlerName)
{
    QString path=_path;

    if(!pathSpace.isNull() && !pathSpace.isEmpty())
        path='/' + pathSpace + _path;

    QString functionName=handlerName;


    return pathTree->registerAPath(path,this,functionName.append("(HttpRequest&,HttpResponse&)"),PathTreeNode::POST);

}

Session & WebApp::startNewSession(HttpRequest & request, unsigned int uid, qint64 _expirationTimeInSecond)
{
    SessionEngine::getSingleton().addNewSession(uid, _expirationTimeInSecond, request.getSession());

    return request.getSession();
}

void WebApp::logOffSession(HttpRequest & request)
{
    SessionEngine::getSingleton().removeSession(request.getSession().getSessionId());

    request.getSession() = Session();
}
