#ifndef UTILITYSERVICE_H
#define UTILITYSERVICE_H

#include "webapp.h"

class UtilityService : public WebApp
{
    Q_OBJECT

public:
    void registerPathHandlers();
    void init();

    void Serve404(HttpRequest &, HttpResponse &);

public slots:
    void handleHelloWorldGet(HttpRequest &,HttpResponse &);
    void gameTitleGet(HttpRequest &,HttpResponse &response);
    void loginHandle(HttpRequest &,HttpResponse &response);
};

#endif // UTILITYSERVICE_H
