#include "sessionengine.h"
#include <QtCore/QDebug>

SessionEngine::SessionEngine(QObject *parent) :
    QObject(parent)
{
}

/*

    void addNewSession(unsigned int uid);
    void removeSession(const QString sessionId);
    void updateSession(const Session &session);
    void getSession(const QString sessionId, const Session &session);
*/

void SessionEngine::addNewSession(unsigned int _uid, qint64 _expirationTimeInSecond, Session &_session)
{
    QUuid sessionUuid = QUuid::createUuid();

    sessionLock.lockForRead();
    while(sessions.contains(sessionUuid.toString()))
    {
        sessionLock.unlock();
        sessionUuid = QUuid::createUuid();
        sessionLock.lockForRead();
    }
    sessionLock.unlock();

    Session session(sessionUuid.toString(),_uid, _expirationTimeInSecond);
    session.setSessionStartTime();

    sessionLock.lockForWrite();

    if (uids.contains(_uid))
    {
        QString oldSessionId = uids[_uid];
        sessions.remove(oldSessionId);
        uids.remove(_uid);
    }

    sessions.insert(session.getSessionId(), session);
    uids.insert(_uid, session.getSessionId());
    sessionLock.unlock();

    _session = session;


}

void SessionEngine::removeSession(const QString &sessionId)
{
    sessionLock.lockForWrite();
    if (sessions.contains(sessionId))
    {
        unsigned int uid = sessions[sessionId].getUId();
        sessions.remove(sessionId);
        uids.remove(uid);
    }
    sessionLock.unlock();
}

void SessionEngine::updateSession(const Session &session)
{
    sessionLock.lockForWrite();
    if (sessions.contains(session.getSessionId()))
    {
        sessions[session.getSessionId()] = session;
    }
    sessionLock.unlock();
}

void SessionEngine::getSession(const QString &sessionId, Session &session)
{
    sessionLock.lockForRead();
    if (sessions.contains(sessionId))
    {
        session = sessions[sessionId];
    }
    sessionLock.unlock();
}

SessionEngine::~SessionEngine()
{

}
