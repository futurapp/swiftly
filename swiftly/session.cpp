#include "session.h"

Session::Session()
    :QObject(),
      m_hasModified(false),
      sessionId(),
      uid(-1),
      userData(),
      expirationTimeInSeconds(-1)
{
}

Session::Session(const QString _sessionId, unsigned int _uid, qint64 _expirationTimeInSeconds)
    :QObject(),
      sessionId(_sessionId),
      uid(_uid),
      expirationTimeInSeconds(_expirationTimeInSeconds),
      m_hasModified(false),
      userData()
{
}

Session::Session(const Session &_in)
    :QObject(),
      sessionId(_in.sessionId),
      uid(_in.uid),
      userData(_in.userData),
      m_hasModified(_in.m_hasModified),
      sessionStartTime(_in.sessionStartTime),
      expirationTimeInSeconds(_in.expirationTimeInSeconds)
{
}

void Session::operator=(const Session &_in)
{
    m_hasModified = _in.m_hasModified;
    sessionId = _in.sessionId;
    uid = _in.uid;
    userData = _in.userData;
    sessionStartTime = _in.sessionStartTime;
    expirationTimeInSeconds = _in.expirationTimeInSeconds;
}

void Session::setUserData(const QString &key, const QVariant &value)
{
    m_hasModified = true;
    userData.insert(key, value);
}

const QVariant & Session::getUserDataForKey(const QString &key)
{
    return userData[key];
}

void Session::removeUserDataForKey(const QString &key)
{
    m_hasModified = true;
    userData.remove(key);
}

bool Session::isExpired()
{
    if (expirationTimeInSeconds > 0)
    {
        if (QDateTime::currentDateTime().secsTo(sessionStartTime) > expirationTimeInSeconds)
        {
            return true;
        }
    }

    return false;
}

void Session::setSessionStartTime()
{
    sessionStartTime = QDateTime::currentDateTime();
}
