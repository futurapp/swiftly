#include "utilityservice.h"


void UtilityService::init()
{
}

void UtilityService::registerPathHandlers()
{
   bool result=addGetHandler("/helloworld","handleHelloWorldGet");
   result |= addGetHandler("/api/client/gametitles.axd","gameTitleGet");
   result |= addGetHandler("/login", "loginHandle");
   qDebug()<<"result of register handler:"<<result;
}

void UtilityService::handleHelloWorldGet(HttpRequest &request,HttpResponse &response)
{
    response << "hello world!\n";

    for(QMap<QString, QString>::Iterator iter= request.getHeader().getQueries().begin(); iter != request.getHeader().getQueries().end();++iter)
    {
        response << iter.key() << "=" << iter.value() << "<br />\n";
    }

    for(QMap<QString, QString>::Iterator iter=request.getHeader().getHeaderInfo().begin(); iter != request.getHeader().getHeaderInfo().end(); ++iter)
    {
        response << iter.key() << "=" << iter.value() << "<br />\n";

    }
}

void UtilityService::gameTitleGet(HttpRequest &,HttpResponse &response)
{
    response << "gametitle!";

}

void UtilityService::loginHandle(HttpRequest &request, HttpResponse &response)
{

    for (QMap<QString, QString>::Iterator iter =  request.getHeader().getCookie().begin(); iter!= request.getHeader().getCookie().end();++iter)
    {
        response << iter.key() << "=" << iter.value() <<"<br/>";
    }

    response.addCookie("cat", QString("Lion"));

    if (!request.getSession().isEmpty())
    {
        response << "old Session id:" << request.getSession().getSessionId();
        response << "old Session uid:" << QString("%1").arg(request.getSession().getUId());
    }
    else
    {
        Session &session = startNewSession(request, qrand());

        response << "new Session id:" << session.getSessionId() << "<br/>";
        response << "new Session uid:" << QString("%1").arg(session.getUId()) << "<br/>";
    }
}


void UtilityService::Serve404(HttpRequest &, HttpResponse &response)
{
    response << "404 Not Found!";
}

