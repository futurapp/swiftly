#ifndef AUTHENTICATION_H
#define AUTHENTICATION_H

#include "webapp.h"
#include <QtSql/QSqlDatabase>

class Authentication : public WebApp
{
    Q_OBJECT

    QByteArray cachedLoginForm;
    QSqlDatabase db;

public:
    void registerPathHandlers();
    void init();

public slots:
    void loginForm(HttpRequest &,HttpResponse &);
    void loginSubmit(HttpRequest &,HttpResponse &);
    void registerForm(HttpRequest &,HttpResponse &response);
    void registerSubmit(HttpRequest &,HttpResponse &response);
    void updateInfoForm(HttpRequest &,HttpResponse &response);
    void updateInfoSubmit(HttpRequest &,HttpResponse &response);

    void loginApi(HttpRequest &,HttpResponse &);
    void registerApi(HttpRequest &,HttpResponse &response);
    void updateInfoApi(HttpRequest &,HttpResponse &response);
};

#endif // AUTHENTICATION_H
