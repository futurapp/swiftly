#ifndef SESSIONENGINE_H
#define SESSIONENGINE_H

#include <QObject>
#include "session.h"
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QReadWriteLock>
#include <QtCore/QUuid>

class SessionEngine : public QObject
{
    Q_OBJECT

    QReadWriteLock sessionLock;
    QMap<unsigned int, QString> uids;
    QMap<QString, Session> sessions;

private:
    explicit SessionEngine(QObject *parent = 0);
    ~SessionEngine();

public:
    static SessionEngine & getSingleton()
    {
        static SessionEngine obj;
        return obj;
    }

    void addNewSession(unsigned int uid, qint64 _expirationTimeInSecond, Session &session);
    void removeSession(const QString &sessionId);
    void updateSession(const Session &session);
    void getSession(const QString &sessionId, Session &session);
    
signals:
    
public slots:
    
};

#endif // SESSIONENGINE_H
