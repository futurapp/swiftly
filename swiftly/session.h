#ifndef SESSION_H
#define SESSION_H

#include <QObject>
#include "session.h"
#include <QtCore/QMap>
#include <QtCore/QVariant>
#include <QtCore/QString>
#include <QtCore/QDateTime>

class Session : public QObject
{
    Q_OBJECT

    bool m_hasModified;
    QString sessionId;
    unsigned int uid;
    QMap<QString, QVariant> userData;
    QDateTime sessionStartTime;
    qint64 expirationTimeInSeconds;

public:
    Session();
    Session(const QString _sessionId, unsigned int _uid, qint64 _expirationTimeInSeconds = -1);
    Session(const Session &_in);
    void operator=(const Session &_in);

    void clearModificationState() {m_hasModified = false;}
    bool hasModified() {return m_hasModified;}
    unsigned int getUId() const {return uid;}
    const QString & getSessionId() const {return sessionId;}
    void setSessionStartTime();

    void setUserData(const QString &key, const QVariant &value);
    const QVariant &getUserDataForKey(const QString &key);
    void removeUserDataForKey(const QString &key);
    bool isExpired();
    bool isEmpty() {return sessionId.isEmpty();}
};

#endif // SESSION_H
