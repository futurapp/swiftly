#ifndef DATABASEENGINE_H
#define DATABASEENGINE_H

#include <QtSql/QSqlDatabase>

class DatabaseEngine : public QObject
{
    Q_OBJECT

private:
    DatabaseEngine();
    ~DatabaseEngine();

public:
    static DatabaseEngine & getSingleton()
    {
        static DatabaseEngine obj;
        return obj;
    }

    void open();

    void addNewUser();

    bool login();

    void close();
};

#endif // DATABASEENGINE_H
